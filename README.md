# DBIS Jupyter Profile Definition
## Jupyter Profile for RWTH Aachen jupyter hub

## Contacts
* Laurenz Neumann - Organisator for DBIS VL 2024 Exercises
* Max Kissgen - Organisator for DBIS VL 2024 Exercises
* Michal Slupczynski - Organisator for DBIS VL 2024 Exercises
* Marcus Meyer - IT Services for RWTH Aachen jupyter hub

## Useful Links
* https://jupyter.rwth-aachen.de
* https://jupyter.rwth-aachen.de/hub/home
* https://git.rwth-aachen.de/jupyter/profiles/-/blob/master/profiles/rwth/courses/dbis.yaml
* https://git.rwth-aachen.de/i5/teaching/dbis-jupyter/dbis-ss-2024