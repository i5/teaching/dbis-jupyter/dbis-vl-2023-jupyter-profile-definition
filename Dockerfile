# if you cannot login, use 
# docker login registry.git.rwth-aachen.de
# this is basically FROM jupyter/minimal-notebook:lab-3.1.18 with additions
ARG BASE_IMAGE=registry.git.rwth-aachen.de/jupyter/profiles/rwth-courses:latest
FROM ${BASE_IMAGE} as base

# Gremlin Server image
FROM tinkerpop/gremlin-server:latest as gremlin-server

# Start Gremlin Server
FROM base

# Copy Gremlin Server files from gremlin-server image
COPY --from=gremlin-server /opt /opt

# Configure shell for conda and update conda to latest version
RUN conda init bash && \
    conda update  --yes conda && \
    conda update  --yes --all

# Install Python3.10
RUN conda install  --yes python=3.10 openjdk=11

# Upgrade to the latest version of sqlite3
RUN conda install  --yes -c anaconda sqlite

# Install graphviz
USER root
RUN apt update && apt install -y \
    graphviz gcc \
    && apt clean && \
	rm -rf /var/lib/apt/lists/*

 
RUN pip install --no-input graphviz

# Install dbis extensions
RUN pip install --no-input \
    "dbis-exc-manager~=1.0" \
    dbis-er-diagram \
    dbis-btree \
    "dbis-functional-dependencies==1.0.0" \
    "dbis-tm==2.0.1" \
    dbis-relational-algebra \
    dbis-relational-model \
    dbis-relational-calculus \
    ipython-sql

# Install nbgitpuller
RUN pip install nbgitpuller
